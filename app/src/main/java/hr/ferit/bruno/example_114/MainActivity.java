package hr.ferit.bruno.example_114;

import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements GestureOverlayView.OnGesturePerformedListener {

    GestureLibrary mGestures;
    @BindView(R.id.ivCat) ImageView ivCat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GestureOverlayView gesturesView = new GestureOverlayView(this);
        View inflate = getLayoutInflater().inflate(R.layout.activity_main, null);
        gesturesView.addView(inflate);
        gesturesView.addOnGesturePerformedListener(this);
        mGestures = GestureLibraries.fromRawResource(this, R.raw.gestures);
        if(!mGestures.load()){ finish(); }
        setContentView(gesturesView);
        ButterKnife.bind(this);
    }

    @Override
    public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {
        ArrayList<Prediction> predictions = mGestures.recognize(gesture);
        int index = 0;
        double maxScore = predictions.get(index).score;
        for (int i=1; i<predictions.size(); i++) {
            if(predictions.get(i).score > maxScore){
                index = i; maxScore = predictions.get(i).score;
            }
        }
        Prediction p = predictions.get(index);
        if(p.name.equalsIgnoreCase("pet"))
            ivCat.setImageResource(R.drawable.happy);
        if(p.name.equalsIgnoreCase("hit"))
            ivCat.setImageResource(R.drawable.scared);
        if(p.name.equalsIgnoreCase("lock"))
            ivCat.setImageResource(R.drawable.sad);

        Toast.makeText(this, p.name + "\n" + p.score, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
